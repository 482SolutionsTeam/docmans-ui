export const Permissions = Object.freeze({
  Owner: "owner",
  View: "view",
  Edit: "edit",
});